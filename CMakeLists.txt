cmake_minimum_required(VERSION 3.3)
project(CPP_Portfolio)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES
        main.cpp
        PFSA_1/my_stack.h
        PFSA_1/my_queue.h
        PFSA_1/my_dbl_e_queue.h
        PFSA_2/my_vector.h
        PFSA_3/my_cpx_num.h
        PFSA_4/my_polynomial.h
        PFSA_4/my_polynomial.cpp
        PFSA_5/room_mode_calc.cpp
        PFSA_5/room_mode_calc.h)
add_executable(CPP_Portfolio ${SOURCE_FILES})