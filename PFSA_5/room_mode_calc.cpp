//----------------------------------------------------------------------
// Implementation for class RoomModeCalculator
//----------------------------------------------------------------------

#include "room_mode_calc.h"
#include <cmath>
#include <iomanip>
#include <fstream>
#include <sstream>

RoomModeCalculator::RoomModeCalculator(float L, float W, float H) :
        Length(L),
        Width(W),
        Height(H),
        tempC(20),
        speed_of_sound(get_sos(tempC)) {
    RunCalculator();
}

RoomModeCalculator::RoomModeCalculator(float L, float W, float H, float in_temp) :
        Length(L),
        Width(W),
        Height(H),
        tempC(in_temp),
        speed_of_sound(343.5) {
    RunCalculator();
}

void RoomModeCalculator::RunCalculator() {
    for (int p = 0; p <= modeLimit; p++) {
        for (int q = 0; q <= modeLimit; q++) {
            for (int r = 0; r <= modeLimit; r++) {
                p_val = (powf(p, 2) / powf(Length, 2));
                q_val = (powf(q, 2) / powf(Width, 2));
                r_val = (powf(r, 2) / powf(Height, 2));
                temp_calc_freq = ((speed_of_sound / 2) * sqrtf(p_val + q_val + r_val));

                if ((p != 0 && q == 0 && r == 0) || (p == 0 && q != 0 && r == 0) || (p == 0 && q == 0 && r != 0)) {
                    axial_list.push_front(temp_calc_freq);
                }

                else if ((p != 0 && q != 0 && r == 0) || (p != 0 && q == 0 && r != 0) || (p == 0 && q != 0 && r != 0)) {
                    tangential_list.push_front(temp_calc_freq);
                }

                else if (p != 0 && q != 0 && r != 0) {
                    oblique_list.push_front(temp_calc_freq);
                }
            }
        }
    }
    axial_list.sort();
    tangential_list.sort();
    oblique_list.sort();
    AnalyzeProblemFreqs();
    axial_problem_freq_list.sort();
}

void RoomModeCalculator::AnalyzeProblemFreqs() {
    for (std::list<float>::iterator it = axial_list.begin(); it != axial_list.end(); it++) {
        if (it != axial_list.begin()) {

            float freq_diff = *it - *prev(it);
            float percent_diff = *it * 0.05;
            if (freq_diff < percent_diff || freq_diff > GilfordLimit) {
                axial_problem_freq_list.push_front(*it);
            }
        }
    }
}

float RoomModeCalculator::get_sos(float in_temp) {
    return float(331 + (0.6 * in_temp));
}

void RoomModeCalculator::PrintAxialList() {
    std::cout << "\nAxial Mode List: " << std::endl;
    for (std::list<float>::iterator it = axial_list.begin(); it != axial_list.end(); it++) {
        if (it == axial_list.begin()) {
            std::cout << "(" << *it;
        }

        else if (next(it) == axial_list.end()) {
            std::cout << ", " << *it << ")";
        }

        else {
            std::cout << ", " << *it;
        }
    }
}

void RoomModeCalculator::PrintTangentialList() {
    std::cout << "\nTangential Mode List: " << std::endl;

    for (std::list<float>::iterator it = tangential_list.begin(); it != tangential_list.end(); it++) {
        if (it == tangential_list.begin()) {
            std::cout << "(" << *it;
        }

        else if (next(it) == tangential_list.end()) {
            std::cout << ", " << *it << ")";
        }

        else {
            std::cout << ", " << *it;
        }
    }
}

void RoomModeCalculator::PrintObliqueList() {
    std::cout << "\nOblique Mode List: " << std::endl;

    for (std::list<float>::iterator it = oblique_list.begin(); it != oblique_list.end(); it++) {
        if (it == oblique_list.begin()) {
            std::cout << "(" << *it;
        }

        else if (next(it) == oblique_list.end()) {
            std::cout << ", " << *it << ")";
        }

        else {
            std::cout << ", " << *it;
        }
    }
}

void RoomModeCalculator::PrintAxialProblemFreqList() {
    std::cout << "\nPotential Axial Mode Problem Frequencies: " << std::endl;

    for (std::list<float>::iterator it = axial_problem_freq_list.begin(); it != axial_problem_freq_list.end(); it++) {
        if (it == axial_problem_freq_list.begin()) {
            std::cout << "(" << *it;
        }

        else if (next(it) == axial_problem_freq_list.end()) {
            std::cout << ", " << *it << ")";
        }

        else {
            std::cout << ", " << *it;
        }
    }
}

void RoomModeCalculator::PrintAllLists() {

    PrintAxialList();

    PrintTangentialList();

    PrintObliqueList();

    PrintAxialProblemFreqList();

}

void RoomModeCalculator::CreateProblemFreqsWav(){
    for(std::list<float>::iterator it = axial_problem_freq_list.begin(); it != axial_problem_freq_list.end(); it++){
        GenerateWav(*it);
    }
}

void RoomModeCalculator::GenerateWav(double in_freq) {

    std::ostringstream file_os;
    file_os << in_freq << "Hz_tone.wav";
    std::string s = file_os.str();
    std::cout <<"\nGenerating " << s << "..."<< std::endl;

    std::ofstream f(s, std::ios::binary);

    // Write the file headers
    f << "RIFF----WAVEfmt ";     // (chunk size to be filled in later)
    write_word(f, 16, 4);  // no extension data
    write_word(f, 1, 2);  // PCM - integer samples
    write_word(f, 2, 2);  // two channels (stereo file)
    write_word(f, 44100, 4);  // samples per second (Hz)
    write_word(f, 176400, 4);  // (Sample Rate * BitsPerSample * Channels) / 8
    write_word(f, 4, 2);  // data block size (size of two integer samples, one for each channel, in bytes)
    write_word(f, 16, 2);  // number of bits per sample (use a multiple of 8)

    // Write the data chunk header
    size_t data_chunk_pos = f.tellp();
    f << "data----";  // (chunk size to be filled in later)

    // Write the audio samples
    constexpr double two_pi = 6.283185307179586476925286766559;
    constexpr double max_amplitude = 32760;  // "volume"

    double hz = 44100;    // samples per second
    double frequency = in_freq;  //Frequency for Argument
    double seconds = 3;      // Length in Seconds

    int N = hz * seconds;  // total number of samples
    for (int n = 0; n < N; n++) {
        double amplitude = (double) n / N * max_amplitude;
        double value = sin((two_pi * n * frequency) / hz);
        write_word(f, (int) (amplitude * value), 2);
        write_word(f, (int) ((max_amplitude - amplitude) * value), 2);
    }

    // Get Final File Size
    // Correct Chunk Size
    size_t file_length = f.tellp();

    // Fix the data chunk header to contain the data size
    // Rewrite Data Chunk Header to contain total data size
    f.seekp(data_chunk_pos + 4);
    write_word(f, file_length - data_chunk_pos + 8);

    // Fix the file header to contain the proper RIFF chunk size, which is (file size - 8) bytes
    // Fix the file header to contain RIFF Chunk Size: (file size - 8) bytes
    f.seekp(0 + 4);
    write_word(f, file_length - 8, 4);

    f.close();
}