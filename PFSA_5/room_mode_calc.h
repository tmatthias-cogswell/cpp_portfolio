//----------------------------------------------------------------------
// RoomModeCalc:
// A class used to calculate potential problem frequencies due to
// standing waves in a given rectangular room.
//----------------------------------------------------------------------

#ifndef CPP_PORTFOLIO_ROOM_MODE_CALC_H
#define CPP_PORTFOLIO_ROOM_MODE_CALC_H

#include <list>
#include <iostream>

class RoomModeCalculator {
public:
    RoomModeCalculator(float, float, float);

    RoomModeCalculator(float, float, float, float);

    void RunCalculator();

    void AnalyzeProblemFreqs();

    float get_sos(float in_temp);

    void PrintAxialList();

    void PrintTangentialList();

    void PrintObliqueList();

    void PrintAxialProblemFreqList();

    void PrintAllLists();

    void CreateProblemFreqsWav();

private:
    static const int modeLimit = 4;
    static const int GilfordLimit = 20;
    float Length;
    float Width;
    float Height;
    float tempC;
    float speed_of_sound;
    float p_val;
    float q_val;
    float r_val;
    float temp_calc_freq;
    std::list<float> axial_list;
    std::list<float> tangential_list;
    std::list<float> oblique_list;
    std::list<float> axial_problem_freq_list;

    template<typename Word>
    std::ostream &write_word(std::ostream &outs, Word value, unsigned size = sizeof(Word)) {
        for (; size; --size, value >>= 8)
            outs.put(static_cast <char> (value & 0xFF));
        return outs;
    }

    void GenerateWav(double in_freq);
};

#endif //CPP_PORTFOLIO_ROOM_MODE_CALC_H
