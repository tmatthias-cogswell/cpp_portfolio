//----------------------------------------------------------------------
// A simple queue template, using a doubly linked list.
//----------------------------------------------------------------------
// TODO: Add Helpful Comments

#ifndef CPP_PORTFOLIO_MY_QUEUE_H
#define CPP_PORTFOLIO_MY_QUEUE_H

#include <iostream>

using std::ostream;

template<class nodeType>
class MyQueue;

template<class nodeType>
ostream &operator<<(ostream &, const MyQueue<nodeType> &);

template<class nodeType>
class MyQueue {
private:
    struct Node {
        nodeType data;
        struct Node *next;
        struct Node *prev;
    };
    Node *start;
    Node *end;
    int count;

public:

    MyQueue();

    MyQueue(int initSize);

    ~MyQueue();

    MyQueue(const MyQueue &);

    void push(const nodeType &);

    void pop();

    MyQueue<nodeType> &operator+=(const MyQueue<nodeType> &);

    MyQueue<nodeType> &operator+(const MyQueue<nodeType> &);

    bool operator==(const MyQueue<nodeType> &);

    friend ostream &operator<<<>(ostream &, const MyQueue<nodeType> &);

};

template<class nodeType>
MyQueue<nodeType>::MyQueue() {
    count = 0;
    start = end = nullptr;
}

template<class nodeType>
MyQueue<nodeType>::~MyQueue() {
    for (int i = 0; i < count; i++) {
        Node *iter = start->next;
        delete start;
        start = iter;
    }
}

template<class nodeType>
MyQueue<nodeType>::MyQueue(const MyQueue<nodeType> &queue) {
    MyQueue<nodeType> *N_Q = new MyQueue<nodeType>;
    Node *iter = queue.end;
    for (int i = 0; i < queue.count; i++) {
        N_Q->push(iter->data);
        iter = iter->prev;
    }
    *this = *N_Q;
}

template<class nodeType>
void MyQueue<nodeType>::push(const nodeType &in_q_data)    // add item to end
{
    if (count == 0) {
        Node *add = new Node;
        add->data = in_q_data;
        add->next = nullptr;
        add->prev = nullptr;
        start = end = add;
        count++;
    }

    else if (count > 0) {
        Node *add = new Node;
        add->data = in_q_data;
        add->next = start;
        add->prev = nullptr;
        add->next->prev = add;
        start = add;

        count++;
    }
}

template<class nodeType>
void MyQueue<nodeType>::pop() {
    delete end;
    count--;
    Node *iter = start;

    for (int i = 1; i <= count; i++) {
        if (i == (count)) {
            end = iter;
            end->next = nullptr;
        }

        else {
            iter = iter->next;
        }
    }
}

template<class nodeType>
MyQueue<nodeType> &MyQueue<nodeType>::operator+=(const MyQueue<nodeType> &r_h_stack) {
    Node *iter = r_h_stack.end;
    int init_count = r_h_stack.count;
    for (int i = 0; i < init_count; i++) {
        this->push(iter->data);
        iter = iter->prev;
    }
    return *this;
}

template<class nodeType>
MyQueue<nodeType> &MyQueue<nodeType>::operator+(const MyQueue<nodeType> &r_h_stack) {
    *this += r_h_stack;
    return *this;
}

template<class nodeType>
bool MyQueue<nodeType>::operator==(const MyQueue<nodeType> &in_queue) {
    if (count != in_queue.count) {
        return false;
    }

    else {
        Node *iter = start;
        Node *in_iter = in_queue.start;

        for (int i = 1; i <= count && in_queue.count; i++) {
            if (iter->data != in_iter->data) {
                return false;
            }

            else if (iter->data == in_iter->data) {
                iter = iter->next;
                in_iter = in_iter->next;
            }
        }
        return true;
    }
}

template<class nodeType>
ostream &operator<<(ostream &out_stream, const MyQueue<nodeType> &out_queue) {
    typename MyQueue<nodeType>::Node *iter = out_queue.start;

    for (int i = 1; i <= out_queue.count; i++) {
        if (i == 1) {
            std::cout << "(" << iter->data << ", ";
        }

        else if (i == out_queue.count) {
            std::cout << iter->data << ")";
        }

        else {
            std::cout << iter->data << ", ";
        }
        iter = iter->next;
    }
    return out_stream;
}

#endif