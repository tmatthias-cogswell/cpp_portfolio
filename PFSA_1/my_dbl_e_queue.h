//----------------------------------------------------------------------
// A simple Double Ended Queue template.
//----------------------------------------------------------------------
// TODO: Add Helpful Comments

#ifndef CPP_PORTFOLIO_MY_DEQUEUE_H
#define CPP_PORTFOLIO_MY_DEQUEUE_H

#include <iostream>

using std::ostream;

template<class nodeType>
class MyDblEQueue;

template<class nodeType>
ostream &operator<<(ostream &, const MyDblEQueue<nodeType> &);

template<class nodeType>
class MyDblEQueue {
private:
    struct Node {
        nodeType data;
        struct Node *next;
        struct Node *prev;
    };
    Node *start;
    Node *end;
    int count;

public:
    MyDblEQueue();

    ~MyDblEQueue();

    MyDblEQueue(const MyDblEQueue &);

    void pushStart(const nodeType &);

    void popStart();

    void pushEnd(const nodeType &);

    void popEnd();

    MyDblEQueue<nodeType> &operator+=(const MyDblEQueue<nodeType> &);

    MyDblEQueue<nodeType> &operator+(const MyDblEQueue<nodeType> &);

    bool operator==(const MyDblEQueue<nodeType> &);

    friend ostream &operator<<<>(ostream &, const MyDblEQueue<nodeType> &);

};

template<class nodeType>
MyDblEQueue<nodeType>::MyDblEQueue() {
    count = 0;
    start = end = nullptr;
}

template<class nodeType>
MyDblEQueue<nodeType>::~MyDblEQueue() {
    for (int i = 0; i < count; i++) {
        Node *iter = start->next;
        delete start;
        start = iter;
    }
}

template<class nodeType>
MyDblEQueue<nodeType>::MyDblEQueue(const MyDblEQueue<nodeType> &dblEQueue) {
    MyDblEQueue<nodeType> *N_Dbl_E = new MyDblEQueue<nodeType>;
    Node *iter = dblEQueue.end;
    for (int i = 0; i < dblEQueue.count; i++) {
        N_Dbl_E->pushStart(iter->data);
        iter = iter->prev;
    }
    *this = *N_Dbl_E;
}

template<class nodeType>
void MyDblEQueue<nodeType>::pushStart(const nodeType &in_data) {
    if (count == 0) {
        Node *add = new Node;
        add->data = in_data;
        add->next = nullptr;
        add->prev = nullptr;
        start = end = add;
        count++;
    }

    else if (count > 0) {
        Node *add = new Node;
        add->data = in_data;
        add->next = start;
        add->next->prev = add;
        start = add;
        count++;
    }
}

template<class nodeType>
void MyDblEQueue<nodeType>::pushEnd(const nodeType &in_data) {
    if (count == 0) {
        Node *add = new Node;
        add->data = in_data;
        add->next = nullptr;
        add->prev = nullptr;
        start = end = add;
        count++;
    }

    else if (count > 0) {
        Node *add = new Node;
        add->data = in_data;
        add->next = nullptr;
        add->prev = end;
        add->prev->next = add;
        end = add;
        count++;
    }
}

template<class nodeType>
void MyDblEQueue<nodeType>::popStart() {
    Node *iter = start->next;
    delete start;
    count--;
    start = iter;
    start->prev = nullptr;

    for (int i = 1; i <= count; i++) {
        if (i == (count)) {
            end = iter;
            end->next = nullptr;
        }

        else {
            iter = iter->next;
        }
    }
}

template<class nodeType>
void MyDblEQueue<nodeType>::popEnd() {
    delete end;
    count--;
    Node *iter = start;

    for (int i = 1; i <= count; i++) {
        if (i == (count)) {
            end = iter;
            end->next = nullptr;
        }

        else {
            iter = iter->next;
        }
    }
}

template<class nodeType>
MyDblEQueue<nodeType> &MyDblEQueue<nodeType>::operator+=(const MyDblEQueue<nodeType> &r_h_dbl_e_queue) {
    Node *iter = r_h_dbl_e_queue.start;
    int init_count = r_h_dbl_e_queue.count;
    for (int i = 0; i < init_count; i++) {
        this->pushStart(iter->data);
        iter = iter->next;
    }
    return *this;
}

template<class nodeType>
MyDblEQueue<nodeType> &MyDblEQueue<nodeType>::operator+(const MyDblEQueue<nodeType> &r_h_dbl_e_queue) {
    *this += r_h_dbl_e_queue;
    return *this;
}

template<class nodeType>
bool MyDblEQueue<nodeType>::operator==(const MyDblEQueue<nodeType> &in_queue) {
    if (count != in_queue.count) {
        return false;
    }

    else {
        Node *iter = start;
        Node *in_iter = in_queue.start;

        for (int i = 1; i <= count && in_queue.count; i++) {
            if (iter->data != in_iter->data) {
                return false;
            }

            else if (iter->data == in_iter->data) {
                iter = iter->next;
                in_iter = in_iter->next;
            }
        }
        return true;
    }
}

template<class nodeType>
ostream &operator<<(ostream &out_stream, const MyDblEQueue<nodeType> &out_stack) {
    typename MyDblEQueue<nodeType>::Node *iter = out_stack.start;

    for (int i = 1; i <= out_stack.count; i++) {
        if (i == 1) {
            std::cout << "(" << iter->data << ", ";
        }

        else if (i == out_stack.count) {
            std::cout << iter->data << ")" << std::endl;
        }

        else {
            std::cout << iter->data << ", ";
        }
        iter = iter->next;
    }
    return out_stream;
}

#endif //CPP_PORTFOLIO_MY_DEQUEUE_H
