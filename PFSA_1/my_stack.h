//----------------------------------------------------------------------
// A simple stack template, using a doubly linked list.
//----------------------------------------------------------------------
// TODO: Add Helpful Comments

#ifndef CPP_PORTFOLIO_MY_STACK_H
#define CPP_PORTFOLIO_MY_STACK_H

#include <iostream>

using std::ostream;

template<class nodeType>
class MyStack;

template<class nodeType>
ostream &operator<<(ostream &, const MyStack<nodeType> &);

template<class nodeType>
class MyStack {
private:
    struct Node {
        nodeType data;
        struct Node *next;
        struct Node *prev;
    };
    Node *start;
    Node *end;
    int count;

public:
    MyStack();

    ~MyStack();

    MyStack(const MyStack &);

    void push(const nodeType &);

    void pop();

    MyStack<nodeType> &operator+=(const MyStack<nodeType> &);

    MyStack<nodeType> &operator+(const MyStack<nodeType> &);

    bool operator==(const MyStack<nodeType> &);

    friend ostream &operator<<<>(ostream &, const MyStack<nodeType> &);

};

template<class nodeType>
MyStack<nodeType>::MyStack() {
    count = 0;
    start = end = nullptr;
}

template<class nodeType>
MyStack<nodeType>::~MyStack() {
    for (int i = 0; i < count; i++) {
        Node *iter = start->next;
        delete start;
        start = iter;
    }
}

template<class nodeType>
MyStack<nodeType>::MyStack(const MyStack<nodeType> &stack) {
    MyStack<nodeType> *N_S = new MyStack<nodeType>;
    Node *iter = stack.end;
    for (int i = 0; i < stack.count; i++) {
        N_S->push(iter->data);
        iter = iter->prev;
    }
    *this = *N_S;
}

template<class nodeType>
void MyStack<nodeType>::push(const nodeType &in_s_data) {
    if (count == 0) {
        Node *add = new Node;
        add->data = in_s_data;
        add->next = nullptr;
        add->prev = nullptr;
        start = end = add;
        count++;
    }

    else if (count > 0) {
        Node *add = new Node;
        add->data = in_s_data;
        add->next = start;
        add->prev = nullptr;
        add->next->prev = add;
        start = add;
        count++;
    }
}

template<class nodeType>
void MyStack<nodeType>::pop() {
    Node *iter = start->next;
    delete start;
    count--;
    start = iter;
    start->prev = nullptr;

    for (int i = 1; i <= count; i++) {
        if (i == (count)) {
            end = iter;
            end->next = nullptr;
        }

        else {
            iter = iter->next;
        }
    }
}

template<class nodeType>
MyStack<nodeType> &MyStack<nodeType>::operator+=(const MyStack<nodeType> &r_h_stack) {
    Node *iter = r_h_stack.start;
    int init_count = r_h_stack.count;
    for (int i = 0; i < init_count; i++) {
        this->push(iter->data);
        iter = iter->next;
    }
    return *this;
}

template<class nodeType>
MyStack<nodeType> &MyStack<nodeType>::operator+(const MyStack<nodeType> &r_h_stack) {
    *this += r_h_stack;
    return *this;
}

template<class nodeType>
bool MyStack<nodeType>::operator==(const MyStack<nodeType> &in_stack) {
    if (count != in_stack.count) {
        return false;
    }

    else {
        Node *iter = start;
        Node *in_iter = in_stack.start;

        for (int i = 1; i <= count && in_stack.count; i++) {
            if (iter->data != in_iter->data) {
                return false;
            }

            else if (iter->data == in_iter->data) {
                iter = iter->next;
                in_iter = in_iter->next;
            }
        }
        return true;
    }
}

template<class nodeType>
ostream &operator<<(ostream &out_stream, const MyStack<nodeType> &out_stack) {
    typename MyStack<nodeType>::Node *iter = out_stack.start;

    for (int i = 1; i <= out_stack.count; i++) {
        if (i == 1) {
            std::cout << "(" << iter->data << ", ";
        }

        else if (i == out_stack.count) {
            std::cout << iter->data << ")";
        }

        else {
            std::cout << iter->data << ", ";
        }
        iter = iter->next;
    }
    return out_stream;
}

#endif