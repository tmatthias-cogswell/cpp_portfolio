//----------------------------------------------------------------------------------------------------
// C++ Portfolio
// By:  Thomas Matthias
//
// A selection of works for SWE315 C++ Object Oriented Programming.
// PFSA 1: Includes templated classes for stacks, queues, and double-ended queues.
// PFSA 2: Includes a templated vector class.
// PFSA 3: Includes a complex number class.
// PFSA 4: A Polynomial Number Class
// TODO: PFSA 5, A Custom Example Project
//----------------------------------------------------------------------------------------------------

#include "PFSA_1/my_queue.h"
#include "PFSA_1/my_stack.h"
#include "PFSA_1/my_dbl_e_queue.h"
#include "PFSA_2/my_vector.h"
#include "PFSA_3/my_cpx_num.h"
#include "PFSA_4/my_polynomial.h"
#include "PFSA_5/room_mode_calc.h"
#include <iostream>

using namespace std;

//----------------------------------------------------------------------------------------------------
// Declaration of Test Functions
//----------------------------------------------------------------------------------------------------

void test_my_stack();

void test_my_queue();

void test_my_dbl_e_queue();

void test_my_vector();

void test_my_cpx_num();

void test_my_polynomial();

void test_room_mode_calc();

//Formatting String
string dashbreak = "\n----------------------------------------\n";

//----------------------------------------------------------------------------------------------------
// Main Runs A Series of Tests that display portfolio functionality.
//----------------------------------------------------------------------------------------------------

int main() {

    test_my_stack();
    test_my_queue();
    test_my_dbl_e_queue();
    test_my_cpx_num();
    test_my_vector();
    test_my_polynomial();
    test_room_mode_calc();
    return 0;
}

//----------------------------------------------------------------------------------------------------
// Test Definitions
//----------------------------------------------------------------------------------------------------

void test_my_stack() {
    MyStack<int> test_s1, test_s2;
    MyStack<char> test_s3;

    for (int i = 0; i < 10; i++) {
        test_s1.push((i + 1) * 5);
        test_s2.push((i + 1) * 10);
    }

    cout << "\nRunning Stack Test:\n" << endl;
    cout << "Test Push" << dashbreak;
    cout << "Stack 1: " << test_s1 << endl;
    cout << "Stack 2: " << test_s2 << endl;

    test_s1.pop();
    test_s2.pop();

    cout << "\nTest Pop" << dashbreak;
    cout << "Stack 1: " << test_s1 << endl;
    cout << "Stack 2: " << test_s2 << endl;

    cout << "\nTest Char Stack" << dashbreak;
    test_s3.push('a');
    test_s3.push('b');
    test_s3.push('c');
    test_s3.push('d');
    cout << test_s3 << endl;

    cout << "\nTest Copy Stacks" << dashbreak;
    cout << "Copying Stack test_s1: ";
    MyStack<int> test_s4(test_s1);
    cout << test_s4 << endl;
    cout << "Copying Stack test_s2: ";
    MyStack<int> test_s5(test_s2);
    cout << test_s5 << endl;

    cout << "\nTest + Stacks" << dashbreak;
    test_s1 = test_s1 + test_s2;
    test_s5 += test_s4;
    cout << "test_s1 + test_s2 = " << test_s1 << endl;
    cout << "test_s5 += test_s4 = " << test_s5 << endl;
    test_s5 += test_s5;
    cout << "test_s5 += test_s5 = " << test_s5 << endl;

    cout << "\nTest == Stacks" << dashbreak;
    MyStack<char> test_s6(test_s3);
    if (test_s3 == test_s6) {
        cout << "True" << endl;
    }

    else {
        cout << "False" << endl;
    }
    cout << "Adding 'e' to test_s6 and re-checking: " << endl;
    test_s6.push('e');
    if (test_s3 == test_s6) {
        cout << "True" << endl;
    }

    else {
        cout << "False" << endl;
    }
}

void test_my_queue() {
    MyQueue<int> test_q1, test_q2;
    MyQueue<char> test_q3;

    for (int i = 0; i < 10; i++) {
        test_q1.push((i + 1) * 2);
        test_q2.push((i + 1) * 4);
    }

    cout << "\nRunning Queue Test:" << endl;
    cout << "\nTest Push" << dashbreak;
    cout << "Stack 1: " << test_q1 << endl;
    cout << "Stack 2: " << test_q2 << endl;

    test_q1.pop();
    test_q2.pop();

    cout << "\nTest Pop" << dashbreak;
    cout << "Stack 1: " << test_q1 << endl;
    cout << "Stack 2: " << test_q2 << endl;

    cout << "\nTest Char Queue\n" << dashbreak;
    test_q3.push('a');
    test_q3.push('b');
    test_q3.push('c');
    test_q3.push('d');
    cout << test_q3 << endl;

    cout << "\nTest Copy Queues\n" << dashbreak;
    cout << "Copying Queue test_q1: ";
    MyQueue<int> test_q4(test_q1);
    cout << test_q4 << endl;
    cout << "Copying Queue test_q2: ";
    MyQueue<int> test_q5(test_q2);
    cout << test_q5 << endl;

    cout << "\nTest + Queues" << dashbreak;
    test_q1 = test_q1 + test_q2;
    test_q5 += test_q4;
    cout << "test_q1 + test_q2 = " << test_q1 << endl;
    cout << "test_q5 += test_q4 = " << test_q5 << endl;
    test_q5 += test_q5;
    cout << "test_q5 += test_q5 = " << test_q5 << endl;

    cout << "\nTest == Queues" << dashbreak;
    MyQueue<char> test_q6(test_q3);
    if (test_q3 == test_q6) {
        cout << "True" << endl;
    }

    else {
        cout << "False" << endl;
    }
    cout << "Adding 'e' to test_s6 and re-checking: " << endl;
    test_q6.push('e');
    if (test_q3 == test_q6) {
        cout << "True" << endl;
    }

    else {
        cout << "False" << endl;
    }
}

void test_my_dbl_e_queue() {
    MyDblEQueue<int> test_deq1;
    MyDblEQueue<char> test_deq2;

    cout << "\nRunning Dbl E Queue Test:  " << endl;
    cout << "\nTest Dbl E Queue Push Start/End" << dashbreak;
    for (int i = 1; i <= 10; i++) {
        test_deq1.pushStart(i * 2);
        test_deq1.pushEnd(i * 3);
    }
    cout << test_deq1 << endl;

    cout << "Test Dbl E Queue Pop Start" << dashbreak;
    test_deq1.popStart();
    cout << test_deq1 << endl;

    cout << "Test Dbl E Queue Pop End" << dashbreak;
    test_deq1.popEnd();
    cout << test_deq1 << endl;

    cout << "Test Char Dbl E Queue" << dashbreak;
    test_deq2.pushStart('a');
    test_deq2.pushEnd('b');
    test_deq2.pushStart('c');
    test_deq2.pushEnd('d');
    cout << test_deq2 << endl;

    cout << "Test Copy Dbl E Queue" << dashbreak;
    MyDblEQueue<int> test_deq3(test_deq1);
    cout << test_deq3 << endl;

    cout << "\nTest == Dbl E Queues" << dashbreak;
    MyDblEQueue<char> test_deq6(test_deq2);
    if (test_deq2 == test_deq6) {
        cout << "True" << endl;
    }

    else {
        cout << "False" << endl;
    }
    cout << "Adding 'e' to test_s6 and re-checking: " << endl;
    test_deq6.pushStart('e');
    if (test_deq2 == test_deq6) {
        cout << "True" << endl;
    }

    else {
        cout << "False" << endl;
    }
}

void test_my_vector() {
    MyVector<int> test_vect(10);
    MyVector<int> test_vect_2(10);
    std::cout << "\nRunning Test To Check Contiguous Memory: " << std::endl;
    if (test_vect.TEST_CONTIG_MEM() == true) {
        std::cout << dashbreak << "Result: Passed!" << std::endl;
    }

    else {
        std::cout << dashbreak << "Result: Failed!" << std::endl;
    }

    for(int i = 0; i < test_vect.getCap(); i++){
        test_vect.add_at_position((i * 2), i);
    }

    for(int i = 0; i < test_vect_2.getCap(); i++){
        test_vect_2.add_at_position((i * 2), i);
    }

    std::cout << "\nTesting Equality" << dashbreak;
    if (test_vect == test_vect_2) {
        std::cout << "Equal!" << std::endl;
    }

    else {
        std::cout << "Not Equal!" << std::endl;
    }

    std::cout << "\nTesting Addition" << dashbreak;
    test_vect = test_vect + test_vect_2;
    std::cout << test_vect << endl;

    std::cout << "\nTesting Subtraction" << dashbreak;
    test_vect = test_vect - test_vect_2;
    std::cout << test_vect << endl;

//    std::cout << "\nTesting Multiplication" << dashbreak;
//    test_vect = test_vect * test_vect_2;
//    std::cout << test_vect << endl;

}

void test_my_cpx_num() {
    MyCpxNum<int> test_cpx_1(7, 10);

    MyCpxNum<int> test_cpx_2(2, 6);

    auto new_cpx = test_cpx_1 / test_cpx_2;
    std::cout << new_cpx << endl;

    MyCpxNum<int> new_cpx_2(test_cpx_1);
    cout << &new_cpx_2 << ": " << new_cpx_2 << endl;
    cout << &test_cpx_1 << ": " << test_cpx_1 << endl;
}

void test_my_polynomial() {
    MyPoly test_poly_1;
    MyPoly test_poly_2;

    test_poly_1.add_element(4, 1);
    test_poly_1.add_element(5, 2);
    test_poly_1.print_elements();
    std::cout << endl;

    test_poly_2.add_element(2, 1);
    test_poly_2.add_element(3, 2);
    test_poly_2.print_elements();
    std::cout << endl;

    //MyPoly test_poly_3 = test_poly_1 * test_poly_2;
    //test_poly_3.print_elements();
    std::cout << endl;
}

void test_room_mode_calc(){
    RoomModeCalculator
            test_RMC(3.33, 2.95, 2.74),
            test_RMC_2(8.6994997216, 6.98499977648001, 3.047999902464);

    std::cout << "\n\ntest_RMC Lists: ";
    test_RMC.PrintAllLists();
    std::cout << "\n\ntest_RMC_2 Lists: ";
    test_RMC_2.PrintAllLists();
    std::cout << "\n\nGenerating Wav Files: " << std::endl;
    test_RMC.CreateProblemFreqsWav();
    test_RMC_2.CreateProblemFreqsWav();
}