#include "my_polynomial.h"

MyPoly::MyPoly() {
    elements = 0;
    first = last = nullptr;
}

MyPoly::~MyPoly() {
    for (int i = 0; i < elements; i++) {
        polyNode *iter = first->next;
        delete first;
        first = iter;
    }
}

void MyPoly::add_element(float in_val, int in_expo) {
    if (elements == 0) {
        polyNode *add = new polyNode;
        add->value = in_val;
        add->expo = in_expo;
        add->next = nullptr;
        add->prev = nullptr;
        first = last = add;
        elements++;
    }

    else if (elements > 0) {
        polyNode *add = new polyNode;
        add->value = in_val;
        add->expo = in_expo;
        add->next = first;
        add->next->prev = add;
        add->prev = nullptr;
        first = add;
        elements++;
    }
}

void MyPoly::print_elements() {
    polyNode *iter = first;

    for(int i = 1; i <= elements; i++){
        std::cout << iter->value << "x^" << iter->expo;
        if(iter->next != nullptr){
            std::cout << " + ";
        }
        iter = iter->next;
    }
}

MyPoly &MyPoly::operator*(const MyPoly &in_poly) {
    MyPoly new_mult_poly;
    polyNode *iter = first;
    polyNode *in_iter = in_poly.first;
    for(int i = 1; i <= elements; i++){
        for(int j = 1; j <= in_poly.elements; i++){
            new_mult_poly.add_element((iter->value * in_iter->value), (iter->expo + in_iter->expo));
            in_iter = in_iter->next;
        }
        iter = iter->next;
    }
    return new_mult_poly;
}
