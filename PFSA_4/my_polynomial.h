//----------------------------------------------------------------------
// A Polynomial Class implemented using a Linked List.
//----------------------------------------------------------------------

#ifndef CPP_PORTFOLIO_MY_POLYNOMIAL_H
#define CPP_PORTFOLIO_MY_POLYNOMIAL_H

#include <iostream>

using std::ostream;

class MyPoly;
ostream &operator<<(ostream &, const MyPoly &);

class MyPoly {
private:
    struct polyNode {
        float value;
        int expo;
        struct polyNode *prev;
        struct polyNode *next;
    };

    polyNode *first;
    polyNode *last;
    int elements;

    void sortNodes();

public:
    MyPoly();

    ~MyPoly();

    MyPoly(const MyPoly &);

    void add_element(float, int);

    void print_elements();

    MyPoly &operator*(const MyPoly &);

    //friend ostream &operator<<(ostream &, const MyPoly &);
};

#endif //CPP_PORTFOLIO_MY_POLYNOMIAL_H
