A collection of works for the final portfolio project, in SWE 315 Object Oriented Programming in C++.  All source code can be accessed by selecting the ["Source"](https://bitbucket.org/tmatthias-cogswell/cpp_portfolio/src) option on the left.

The file **All_Output** contains example output for PFSA_1 - PFSA_5, and the file **PFSA_5 Example Output** contains test output for only PFSA_5.

A compiled execuatable exists in /bin/Release, which when run will generate two sets of .wav files.