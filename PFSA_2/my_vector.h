//----------------------------------------------------------------------
// A simple 1-dimensional vector template.
//----------------------------------------------------------------------

#ifndef CPP_PORTFOLIO_MY_VECTOR_H
#define CPP_PORTFOLIO_MY_VECTOR_H

template<class vectType>
class MyVector;

template<class vectType>
ostream &operator<<(ostream &, const MyVector<vectType> &);

template<class vectType>
class MyVector {
public:
    MyVector(int);

    ~MyVector();

    MyVector(const MyVector &);

    bool operator==(const MyVector &);

    MyVector<vectType> &operator+=(const MyVector<vectType> &);

    MyVector<vectType> &operator+(const MyVector<vectType> &);

    MyVector<vectType> &operator-=(const MyVector &);

    MyVector<vectType> &operator-(const MyVector &);

    MyVector<vectType> &operator*=(int);

    MyVector<vectType> &operator*(int);

    MyVector<vectType> &operator*=(const MyVector &);

    MyVector<vectType> &operator*(const MyVector &);

    void add_at_position(vectType, int);

    bool bound_check(int);

    int getCap();

    friend ostream &operator<<<>(ostream &, const MyVector<vectType> &);

    bool TEST_CONTIG_MEM();

private:
    int v_cap;

    vectType *v_array;
};

template<class vectType>
MyVector<vectType>::MyVector(int start_capacity) : v_cap(start_capacity) {
    v_array = new vectType[start_capacity];
    std::cout << "Creating: " << this << std::endl;
    for (int i = 0; i < v_cap; i++) {
        v_array[i] = 0;
    }
}

template<class vectType>
MyVector<vectType>::~MyVector() {
    delete v_array;
}

template<class vectType>
MyVector<vectType>::MyVector(const MyVector &in_vect) {
    v_array = new vectType[in_vect.v_cap];
    for (int i = 0; i < v_cap; i++) {
        v_array[i] = in_vect.v_array[i];
    }
}


template<class vectType>
bool MyVector<vectType>::operator==(const MyVector &in_vect) {
    for (int i = 0; i < v_cap; i++) {
        if (v_array[i] != in_vect.v_array[i]) {
            return false;
        }
    }
    return true;
}

template<class vectType>
MyVector<vectType> &MyVector<vectType>::operator+=(const MyVector &in_vect) {
    *this += in_vect;
    return *this;
}

template<class vectType>
MyVector<vectType> &MyVector<vectType>::operator+(const MyVector &in_vect) {
    for (int i = 0; i < v_cap; i++) {
        v_array[i] = v_array[i] + in_vect.v_array[i];
    }
    return *this;
}

template<class vectType>
MyVector<vectType> &MyVector<vectType>::operator-=(const MyVector &in_vect) {
    for (int i = 0; i < v_cap; i++) {
        v_array[i] = v_array[i] - in_vect.v_array[i];
    }
    return *this;
}

template<class vectType>
MyVector<vectType> &MyVector<vectType>::operator-(const MyVector &in_vect) {
    *this -= in_vect;
    return *this;
}

template<class vectType>
MyVector<vectType> &MyVector<vectType>::operator*=(int scalar) {
    for (int i = 0; i < v_cap; i++) {
        v_array[i] = v_array[i] * scalar;
    }
    return *this;
}

template<class vectType>
MyVector<vectType> &MyVector<vectType>::operator*(int scalar) {
    *this *= scalar;
    return *this;
}

template<class vectType>
MyVector<vectType> &MyVector<vectType>::operator*=(const MyVector &in_vect) {
    for (int i = 0; i < v_cap; i++) {
        v_array[i] = v_array[i] * in_vect.v_array[i];
    }
    return *this;
}

template<class vectType>
MyVector<vectType> &MyVector<vectType>::operator*(const MyVector &in_vect) {
    *this *= in_vect;
    return *this;
}

template<class vectType>
void MyVector<vectType>::add_at_position(vectType value, int position) {
    if (bound_check(position)) {
        v_array[position] = value;
    }
    else {
        v_array[position] = 0;
        std::cout << "Bad Position @ " << position << ", Value(" << value << ") Not Set!" << std::endl;
    }
}

template<class vectType>
bool MyVector<vectType>::bound_check(int val) {
    return !(val >= v_cap || val < 0);
}

template<class vectType>
int MyVector<vectType>::getCap() {
    return v_cap;
}

template<class valueType>
ostream &operator<<(ostream &out_stream, const MyVector<valueType> &out_vector) {

    for (int i = 0; i < out_vector.v_cap; i++) {
        if (i == 0) {
            std::cout << "(" << out_vector.v_array[i] << ", ";
        }
        else if (i == out_vector.v_cap - 1) {
            std::cout << out_vector.v_array[i] << ")";
        }
        else {
            std::cout << out_vector.v_array[i] << ", ";
        }
    }
    return out_stream;
}

template<class vectType>
bool MyVector<vectType>::TEST_CONTIG_MEM() {
    for (int i = 0; i < v_cap; i++) {
        std::cout << "Address: " << &v_array[i] << std::endl;
        std::cout << "Difference Between: " << (&v_array[i + 1] - &v_array[i]) << std::endl;
        if ((&v_array[i + 1] - &v_array[i]) != 1) {
            return false;
        }
    }
    return true;
}

#endif