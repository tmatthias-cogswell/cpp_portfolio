//----------------------------------------------------------------------
// A simple complex number template, for use with either <int> or <valueType>
//----------------------------------------------------------------------
// TODO: Needs Destructor?

#ifndef CPP_PORTFOLIO_MyCpxNum_H
#define CPP_PORTFOLIO_MyCpxNum_H

#include <iostream>

using std::ostream;

template<class valueType>
class MyCpxNum;

template<class valueType>
ostream &operator<<(ostream &, const MyCpxNum<valueType> &);

template<class valueType>
class MyCpxNum {
private:
    valueType real;
    valueType imag;
public:
    MyCpxNum();

    MyCpxNum(valueType, valueType);

//    ~MyCpxNum();

    MyCpxNum(const MyCpxNum &);

    MyCpxNum<valueType> operator+(const MyCpxNum &);

    MyCpxNum<valueType> operator-(const MyCpxNum &);

    MyCpxNum<valueType> operator*(const MyCpxNum &);

    MyCpxNum<float> operator/(const MyCpxNum &);

    MyCpxNum<valueType> get_conj(const MyCpxNum &);

    friend ostream &operator<<<>(ostream &, const MyCpxNum<valueType> &);

};

template<class valueType>
MyCpxNum<valueType>::MyCpxNum() : real(0), imag(0) { }

template<class valueType>
MyCpxNum<valueType>::MyCpxNum(valueType r_in, valueType i_in) : real(r_in), imag(i_in) { }

template<class valueType>
MyCpxNum<valueType>::MyCpxNum(const MyCpxNum &in_cpx) {
    MyCpxNum<valueType> *N_Cpx = new MyCpxNum<valueType>;
    N_Cpx->real = in_cpx.real;
    N_Cpx->imag = in_cpx.imag;
    *this = *N_Cpx;
}

template<class valueType>
MyCpxNum<valueType> MyCpxNum<valueType>::operator+(const MyCpxNum<valueType> &in_cpx) {
    valueType result_real = real + in_cpx.real;
    valueType result_imag = imag + in_cpx.imag;
    return MyCpxNum(result_real, result_imag);
}

template<class valueType>
MyCpxNum<valueType> MyCpxNum<valueType>::operator-(const MyCpxNum<valueType> &in_cpx) {
    valueType result_real = real - in_cpx.real;
    valueType result_imag = imag - in_cpx.imag;
    return MyCpxNum(result_real, result_imag);
}

template<class valueType>
MyCpxNum<valueType> MyCpxNum<valueType>::operator*(const MyCpxNum<valueType> &in_cpx) {
    valueType result_real = (real * in_cpx.real) - (imag * in_cpx.imag);
    valueType result_imag = (real * in_cpx.imag) + (imag * in_cpx.real);
    return MyCpxNum(result_real, result_imag);
}

template<class valueType>
MyCpxNum<float> MyCpxNum<valueType>::operator/(const MyCpxNum<valueType> &in_cpx) {
    valueType numer_real = (real * in_cpx.real) + (imag * in_cpx.imag);
    valueType numer_imag = (imag * in_cpx.real) - (real * in_cpx.imag);
    float denom = (in_cpx.real * in_cpx.real) * (in_cpx.imag * in_cpx.imag);
    float result_real = numer_real / denom;
    float result_imag = numer_imag / denom;
    return MyCpxNum<float>(result_real, result_imag);
}

template<class valueType>
MyCpxNum<valueType> MyCpxNum<valueType>::get_conj(const MyCpxNum<valueType> &in_cpx) {
    real = in_cpx.real;
    imag = in_cpx.imag * -1;
    return MyCpxNum(real, imag);
}

template<class valueType>
ostream &operator<<(ostream &out_stream, const MyCpxNum<valueType> &out_cpx_num) {
    std::cout << out_cpx_num.real << " + " << out_cpx_num.imag << "i";
    return out_stream;
}

#endif
